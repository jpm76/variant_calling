#/bin/bash
#
#$ -S /bin/bash
#SBATCH --get-user-env
#SBATCH --job-name=test_indelRealigner_two_samples												
#SBATCH --error=/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/indelaligner_test/indelRealigner.err		
#SBATCH --output=/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/indelaligner_test/indelRealigner.out 		
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=END
#SBATCH --mail-user=Julius_Mojica_usr@duke.edu
#SBATCH --workdir=/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/indelaligner_test						
#SBATCH --mem=14G

for sample in *.bam ; do
   describer=$(echo ${sample} | sed 's/.fq.gz.bam//')
   srun java -Xmx14g -jar /opt/apps/slurm/GATK/GenomeAnalysisTK.jar \
   -T IndelRealigner \
   -R /dscrgrps/tmolab/Julius_Mojica_usr/Bstricta_reference/Bstricta_278_v1.hardmasked.fa \
   -I $sample \
   -targetIntervals $describer.forIndelRealigner.intervals \
   -o $describer.realigned.bam
done
