#/bin/bash
#
#$ -S /bin/bash
#SBATCH --get-user-env
#SBATCH --job-name=forIndelRealigner_test 														     
#SBATCH --error=/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/indelaligner_test/forIndelRealigner.fq.gz.err   
#SBATCH --output=/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/indelaligner_test/forIndelRealigner.fq.gz.out  
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=Julius_Mojica_usr@duke.edu
#SBATCH --workdir=/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/indelaligner_test							 
#SBATCH --mem=2G

for sample in *.bam ; do
   describer=$(echo ${sample} | sed 's/.fq.gz.bam//')
   srun java -Xmx2g -jar /opt/apps/slurm/GATK/GenomeAnalysisTK.jar \
   -T RealignerTargetCreator \
   -R /dscrgrps/tmolab/Julius_Mojica_usr/Bstricta_reference/Bstricta_278_v1.hardmasked.fa \
   -I $sample \
   -o $describer.forIndelRealigner.intervals
done
