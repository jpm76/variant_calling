#/usr/bin/env python

import sys
import re


barcode_file= open("key.txt","rU") 
folder_name = "Index1-ATCACG"      
Index_group = sys.argv[1] # only command line argument is name of input fastq file
barcodes_dict = dict()
for idx,row in enumerate(barcode_file):
line = row.replace('\n', '').replace('\r', '').split('\t')
if line[0]==folder_name:
	barcodes_dict[line[1]] = {'name':line[2], 'file_name':line[2]+".fq"}
# if idx < 5: 
# 	print 'row:', row
# 	print 'line:', line
# 	print 'barcodes_dict:', barcodes_dict


#fq_file = open("Sample_"+folder_name+"/"+Index_group+".fq", 'r')
fq_file = open(Index_group, 'r')
current_file = ''


for idx, row in enumerate(fq_file):
line = row.replace('\n', '').replace('\r', '')
if idx%4 == 0:
	name_line = row
elif idx%4 == 1:
	try:
		current_file = open(barcodes_dict[row[:6]]['file_name'], 'a')
		current_file.write(name_line)
		current_file.write(row[6:])

	except KeyError:
		current_file = open(Index_group+'.misc.barcodes.fq', 'a')
		current_file.write(name_line)
		current_file.write(row[6:])
elif idx%4 == 2:
	# print 'idx:', idx
	# print 'row', row
	current_file.write(row)
else:
	current_file.write(row[6:])