#/usr/bin/env python

import sys
import os

commandsFile = open("bwa_all.sh", 'w')
mainDir = '/netscratch/Julius_Mojica_usr/GBS_RefPop_MV_101G/Lane1_index1' 

in0  =open("Lane1_index1_samples.txt", "rU") 
for line_idx, line in enumerate(in0):

fqfile=line.replace('\n', '')
cols = line.replace('\n', '').split('_')


samp1=fqfile+".sam"
samp3=fqfile+".bam"

sampleName = fqfile

shellScriptName = '%s_sub.sh' % (sampleName)
shellScript = open(shellScriptName, 'w' )

commandsFile.write('sbatch %s \n' % (shellScriptName))

shellScript.write("#/bin/bash\n")
shellScript.write("#\n")
shellScript.write("#$ -S /bin/bash\n")
shellScript.write("#SBATCH --get-user-env\n")
shellScript.write("#SBATCH --job-name=j_%s\n" % (sampleName))
shellScript.write("#SBATCH --error=%s/map_%s.err\n" % (mainDir,sampleName))
shellScript.write("#SBATCH --output=%s/map_%s.out\n" % (mainDir,sampleName))
shellScript.write("#SBATCH --ntasks=1\n")
shellScript.write("#SBATCH --cpus-per-task=1\n")
shellScript.write("#SBATCH --mail-type=ALL\n")
shellScript.write("#SBATCH --mail-user=Julius_Mojica_usr@duke.edu\n")
shellScript.write("#SBATCH --workdir=%s\n" % (mainDir))
shellScript.write("#SBATCH --mem=4000\n\n")

shellScript.write("/opt/apps/bin/bwa mem Bstricta_278_v1.hardmasked.fa %s > %s\n" % (fqfile,samp1) )
shellScript.write("/opt/apps/bin/samtools view -bS %s | /opt/apps/bin/samtools sort - %s\n" %(samp1,fqfile) )
# shellScript.write("rm -f %s \n" %(samp1) )

shellScript.write("java -Xmx2048m -jar /dscrgrps/tmolab/Julius_Mojica_usr/picard/picard-tools-1.119/AddOrReplaceReadGroups.jar I=%s O=RG/%s SO=coordinate RGID=SeqRUN# RGLB=%s RGPL=illumina RGPU=%s RGSM=%s VALIDATION_STRINGENCY=LENIENT \n" %(samp3,samp3,samp3,samp3,samp3 ) )
shellScript.write("/opt/apps/bin/samtools index RG/%s RG/%s.bai \n"%(samp3,samp3) )


